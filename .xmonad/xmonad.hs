import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks (ToggleStruts(..),avoidStruts,docks,manageDocks)
import XMonad.Hooks.ManageHelpers

import XMonad.Util.Run(spawnPipe)
import System.IO

import XMonad.Layout.Gaps()
import XMonad.Layout.Tabbed
import XMonad.Layout.Accordion()
import XMonad.Layout.NoBorders

import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import System.Exit

--Para mplabx
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName

--Para ssh
import XMonad.Prompt
import XMonad.Prompt.Ssh

import qualified XMonad.Actions.Submap as SM
import qualified XMonad.Actions.Search as S

myTerminal    = "urxvt -bg black -fg white +sb"

myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"
--mywrk = ["term","gimp","web","email","emacs","6","7","8","9"]

myTabConfig = def { --activeColor = "#556064"
                  -- , inactiveColor = "#2F3D44"
                  -- , urgentColor = "#FDF6E3"
                  -- , activeBorderColor = "#454948"
                  -- , inactiveBorderColor = "#454948"
                  -- , urgentBorderColor = "#268BD2"
                  -- , activeTextColor = "#80FFF9"
                  -- , inactiveTextColor = "#1ABC9C"
                  -- , urgentTextColor = "#1ABC9C"
                  fontName = "xft:DejaVu Sans Mono:pixelsize=12:antialias=true:hinting=true"
                  }


myLayout =  avoidStruts $
  (noBorders Full ||| Mirror tiled ||| tiled ||| tabbed shrinkText myTabConfig)
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

dmenuExec =
    "exe=`dmenu_run | dmenu_run -nb black -nf white -sb gray -sf black` && eval \"exec $exe\""

myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
    [ ((modMask .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
    , ((modMask,               xK_p     ), spawn dmenuExec)
    , ((modMask .|. shiftMask, xK_p     ), spawn "gmrun")
    , ((modMask .|. shiftMask, xK_e     ), spawn "chromium")
    , ((modMask .|. shiftMask, xK_z     ), sshPrompt defaultXPConfig)
    , ((modMask,               xK_e     ), spawn "/home/adelgado/apps/emacs/bin/emacs")
    , ((modMask,               xK_F4    ), spawn "icedove")
    , ((modMask,               xK_F1    ), spawn "xfe")
    , ((controlMask .|. shiftMask, xK_q), spawn "xscreensaver-command -lock")
--    , ((controlMask, xK_Print),           spawn "sleep 0.2; scrot -s ~/screenshots/%F_%T-Ventana.png; notify-send \'Ventana Capturada\'")
--    , ((0, xK_Print),                     spawn "scrot ~/screenshots/%F_%T-FullScreen.png")
    , ((0, xK_Print),                     spawn "flameshot gui -p ~/screenshots")
    -- close focused window
    , ((modMask .|. shiftMask, xK_c     ), kill)
    -- Rotate through the available layout algorithms
    , ((modMask,               xK_space ), sendMessage NextLayout)
    --  Reset the layouts on the current workspace to default
    , ((modMask .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    -- Resize viewed windows to the correct size
    , ((modMask,               xK_n     ), refresh)
    -- Move focus to the next window
    , ((modMask,               xK_Tab   ), windows W.focusDown)
    -- Move focus to the next window
    , ((modMask,               xK_j     ), windows W.focusDown)
    -- Move focus to the previous window
    , ((modMask,               xK_k     ), windows W.focusUp  )
    -- Move focus to the master window
    , ((modMask,               xK_m     ), windows W.focusMaster  )
    -- Swap the focused window and the master window
    , ((modMask,               xK_Return), windows W.swapMaster)
    -- Swap the focused window with the next window
    , ((modMask .|. shiftMask, xK_j     ), windows W.swapDown  )
    -- Swap the focused window with the previous window
    , ((modMask .|. shiftMask, xK_k     ), windows W.swapUp    )
    -- Shrink the master area
    , ((modMask,               xK_h     ), sendMessage Shrink)
    -- Expand the master area
    , ((modMask,               xK_l     ), sendMessage Expand)
    -- Push window back into tiling
    , ((modMask,               xK_t     ), withFocused $ windows . W.sink)
    -- Increment the number of windows in the master area
    , ((modMask              , xK_comma ), sendMessage (IncMasterN 1))
    -- Deincrement the number of windows in the master area
    , ((modMask              , xK_period), sendMessage (IncMasterN (-1)))
    -- Quit xmonad
    , ((modMask .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
    -- Restart xmonad
    , ((modMask              , xK_q     ), restart "xmonad" True)
    , ((mod4Mask             , xK_b     ), sendMessage ToggleStruts)
--    , ((modMask, xK_s), SM.submap $ searchEngineMap $ S.promptSearch P.defaultXPConfig)
--    , ((modMask .|. shiftMask, xK_s), SM.submap $ searchEngineMap $ S.selectSearch)
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modMask, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    -- Parte comentada por mi, para que no me dé la brasa con el mod-w en emacs
    -- Ya que como no tengo soporte para xinerama pos como que me da igual
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_r, xK_w] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "MPlayer"          --> doFloat
    , className =? "Xmessage"         --> doFloat
    , className =? "Klavaro"          --> doFloat
    , className =? "Wine"             --> doFloat
    , className =? "Gimp"             --> doFloat
    , className =? "URxvt"            --> doF (W.shift "1")
    , className =? "Gimp"             --> doF (W.shift "2")
    , className =? "Iceweasel"        --> doF (W.shift "3")
    , className =? "Chromium"         --> doF (W.shift "3")
    , className =? "chromium"         --> doF (W.shift "3")
    , className =? "chromium-browser" --> doF (W.shift "3")
    , className =? "Icedove"          --> doF (W.shift "4")
    , className =? "Thunderbird"      --> doF (W.shift "4")
    , className =? "Emacs"            --> doF (W.shift "5")
    , className =? "Code"             --> doF (W.shift "5")
    , className =? "Skype"            --> doF (W.shift "8")
    , className =? "teams-for-linux"  --> doF (W.shift "9")
    , resource  =? "desktop_window"   --> doIgnore
    , resource  =? "kdesktop"         --> doIgnore
    ]

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- main = xmonad $ ewmh defaultConfig{ handleEventHook =
--            handleEventHook defaultConfig <+> fullscreenEventHook }

main = do
  xmproc <- spawnPipe "/usr/bin/xmobar ~/.xmonad/xmobarrc"
  xmonad $ ewmh $ docks $defaultConfig {
    startupHook = ewmhDesktopsStartup >> setWMName "LG3D",
    modMask = mod4Mask,
    terminal = myTerminal,
    normalBorderColor  = myNormalBorderColor,
    focusedBorderColor = myFocusedBorderColor,
    -- workspaces = mywrk,
    manageHook = ( isFullscreen --> doFullFloat ) <+> manageDocks <+> manageHook defaultConfig <+> myManageHook,
    layoutHook = smartBorders $ myLayout,
    keys = myKeys,
    logHook = dynamicLogWithPP $ xmobarPP
              { ppOutput = hPutStrLn xmproc,
                ppTitle = xmobarColor "green" "" . shorten 50
              }
  }

searchEngineMap method = M.fromList $
        [ ((0, xK_g), method S.google)
        , ((0, xK_c), method S.codesearch)
        , ((0, xK_w), method S.wikipedia)
        , ((0, xK_d), method $ S.searchEngine "duckduckgo" "https://duckduckgo.com/?q=")
        , ((0, xK_r), method $ S.searchEngine "wordreference" "http://www.wordreference.com/es/translation.asp?tranword=")
        , ((0, xK_b), method $ S.searchEngine "rae" "http://rae.es/")
        ]
