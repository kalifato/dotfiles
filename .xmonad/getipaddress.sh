#!/bin/bash

# Prints current IP address
ip=`ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p'`
#ip=`ip route get 1 | awk '{print $NF;exit}'`

echo Ip: `expr $ip`
