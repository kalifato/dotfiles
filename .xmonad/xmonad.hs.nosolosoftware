import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Layout.NoBorders
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Prompt
import XMonad.Prompt.Ssh
import XMonad.Util.XSelection
import qualified XMonad.Prompt         as P
import qualified XMonad.Actions.Submap as SM
import qualified XMonad.Actions.Search as S
import qualified Data.Map as M
import System.IO

myManageHook = composeAll
	[ className =? "Gimp"	--> doFloat 
  , className =? "wine" --> doFloat
  ]


main = do
	xmproc <- spawnPipe "xmobar"
	xmonad $ defaultConfig 
		{ manageHook = manageDocks <+> manageHook defaultConfig
		, layoutHook = smartBorders . avoidStruts  $  layoutHook defaultConfig 
		, logHook = dynamicLogWithPP xmobarPP
			{ ppOutput = hPutStrLn xmproc
			, ppTitle = xmobarColor "green" "" . shorten 80
			}
		, modMask = mod4Mask
		, terminal = "urxvt -e /bin/bash --login -i"
		} `additionalKeys`
		[ ((mod4Mask .|. shiftMask, xK_z), spawn "xscreensaver-command -lock")
		, ((mod4Mask, xK_b), sendMessage ToggleStruts)
		, ((mod4Mask .|. shiftMask, xK_o), promptSelection "firefox")
		, ((mod4Mask, xK_x), sshPrompt defaultXPConfig)
		, ((mod4Mask, xK_s), SM.submap $ searchEngineMap $ S.promptSearch P.defaultXPConfig)
		, ((mod4Mask .|. shiftMask, xK_s), SM.submap $ searchEngineMap $ S.selectSearch)
		, ((0, xK_Print), spawn "scrot")
		]

searchEngineMap method = M.fromList $
	[ ((0, xK_g), method S.google)
	, ((0, xK_c), method S.codesearch)
	, ((0, xK_w), method S.wikipedia)
  , ((0, xK_d), method $ S.searchEngine "duckduckgo" "https://duckduckgo.com/?q=")
	, ((0, xK_r), method $ S.searchEngine "wordreference" "http://www.wordreference.com/es/translation.asp?tranword=")
	, ((0, xK_b), method $ S.searchEngine "rae" "http://rae.es/")
	]
